package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(MockitoExtension.class)
public class TestKinoverwaltung {
    private Vorstellung die_jagd_auf_roter_oktober, spass_Mit_SpringBoot;
    private KinoVerwaltung kinoVerwaltung = new KinoVerwaltung();
    private KinoSaal ks;

    @BeforeEach
    void setup() {
        //Saal anlegen
        Map<Character, Integer> map = new HashMap<>();
        map.put('A', 10);
        map.put('B', 20);
        map.put('C', 15);
        ks = new KinoSaal("Saal1", map);
        die_jagd_auf_roter_oktober = new Vorstellung(ks, Zeitfenster.ABEND, LocalDate.of(2022, 10, 26), "Die Jagd auf Roter Oktober", 10F);
        spass_Mit_SpringBoot = new Vorstellung(ks, Zeitfenster.NACHMITTAG, LocalDate.of(2022, 02, 01), "Spass mit Spring-Boot", 1F);

        kinoVerwaltung.einplanenVorstellung(die_jagd_auf_roter_oktober);
    }

    @Test
    void test_einpalenVorstellungEinplanenMehrfach() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> kinoVerwaltung.einplanenVorstellung(die_jagd_auf_roter_oktober));
    }

    @Test
    void test_einpalenVorstellungMitNichtVorhandenenPlatz() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> kinoVerwaltung.kaufeTicket(die_jagd_auf_roter_oktober, 'P', 8, 10F), "kk");
    }

    @Test
    void testTicketKaufenMitBelegtemPlatz() {


        ArrayList<Throwable> al = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            Random random = new Random();
            int testValue = random.nextInt(20) + 1;
            System.out.println(testValue);
            try {
                Assertions.assertEquals('B', kinoVerwaltung.kaufeTicket(spass_Mit_SpringBoot, 'B', testValue, 10F).getReihe());
            } catch (IllegalArgumentException | IllegalStateException e1) {
                al.add(e1);

            }
        }
        if (al.size() > 0) {
            System.out.println("Ausgabe");
            System.out.println(al);
            // Assertions.fail();
        }
    }

    @TestFactory
    Collection<DynamicTest> testZufaelligVorstellungEinplanen() {

        List<DynamicTest> testListe = new ArrayList<>();

        ArrayList<Throwable> al = new ArrayList<>();
        int i;
        for (i = 0; i < 20; i++) {
            die_jagd_auf_roter_oktober = new Vorstellung(ks, Zeitfenster.ABEND, LocalDate.of(2022, 10, i + 1), "Die Jagd auf Roter Oktober", 10F);

            Random random = new Random();
            int testValue = random.nextInt(20) + 1;
            System.out.println(testValue);
            if (testValue > 5) {
                try {
                    testListe.add(DynamicTest.dynamicTest("Roter Oktober", () -> Assertions.assertEquals('B', kinoVerwaltung.kaufeTicket(die_jagd_auf_roter_oktober, 'B', testValue, 10F).getReihe())));
                } catch (IllegalArgumentException | IllegalStateException e1) {
                    al.add(e1);
                }
            } else {

                try {
                    testListe.add(DynamicTest.dynamicTest("Spas mit Spring-Boot", () -> Assertions.assertEquals("Saal1", kinoVerwaltung.kaufeTicket(spass_Mit_SpringBoot, 'B', testValue, 10F).getSaal())));
                } catch (IllegalArgumentException | IllegalStateException e1) {
                    al.add(e1);

                }
            }

            if (al.size() > 0) {
                System.out.println("Ausgabe");
                System.out.println(al);
                Assertions.fail();
            }

        }
        return testListe;
    }
}
