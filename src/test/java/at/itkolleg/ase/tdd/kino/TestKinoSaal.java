package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class TestKinoSaal {
    @Mock
    private KinoSaal kinosaalMock; //Mocking Stub zum Testen

    private KinoSaal kinosaalOriginal;
    private Map<Character, Integer> map = new HashMap<>();

    @BeforeEach
    void setup() {
        //Saal anlegen
        map.put('A', 10);
        map.put('B', 10);
        map.put('C', 15);
        kinosaalOriginal = new KinoSaal("KS2", map);
    }

    @Test
    void testKinosaalMockName() {
        //wenn vom Stub die getName aufgerufen wird, soll KS1 zurückkommen
        Mockito.when(kinosaalMock.getName()).thenReturn("KS1");
        //Schauen ob der Wert korrekt gemockt wurde
        assertEquals("KS1", kinosaalMock.getName());

        Mockito.verify(kinosaalMock).getName();
    }

    @Test
    void testKinosaalMockName_Falsch() {
        //wenn vom Stub die getName aufgerufen wird, soll KS1 zurückkommen
        Mockito.when(kinosaalMock.getName()).thenReturn("KS1");
        //Schauen ob der Wert korrekt gemockt wurde
        assertEquals("KS1", kinosaalMock.getName(), "Es wurde der Richtige Name mitgegeben");

        Mockito.verify(kinosaalMock).getName();
    }

    @Test
    void testKinosaalName_Setup_gegen_Mock() {
        Mockito.when(kinosaalMock.getName()).thenReturn("KS2");
        assertEquals(kinosaalMock.getName(), kinosaalOriginal.getName());
    }

    @Test
    void testKinosaalPlätze() {
        assertFalse(kinosaalOriginal.pruefePlatz('A', 11));
        assertTrue(kinosaalOriginal.pruefePlatz('A', 10));
        assertTrue(kinosaalOriginal.pruefePlatz('B', 10));
        assertTrue(kinosaalOriginal.pruefePlatz('C', 14));
    }

    @Test
    void testKinosaalName() {
        assertEquals("KS2", kinosaalOriginal.getName());
    }

    @Test
    void testKinosaal() {

        map.put('A', 10);
        map.put('B', 10);
        map.put('C', 15);
        kinosaalMock = new KinoSaal("KS2", map);
        // Mockito.verify(kinosaalOriginal).equals(kinosaalMock);
        kinosaalOriginal.equals(kinosaalMock);
    }
}
