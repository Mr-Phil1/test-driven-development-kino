---
title: Test-Driven Development
subtitle:
author: Mr. Phil
rights: Nah
language: de-AT
keywords: ;
titlepage: true
titlepage-color: "C2BB9B"
titlepage-text-color: "000000"
table-use-row-colors: true
toc-own-page: true
---

# Testdriven Development

- funktionsweise: Ich testen während dem Entwicklen
- **Die Testklassen müssen auf der selben Packageebene sein wie die zu Testende Klasse**

\pagebreak

# Red-Green-Refactor

Die Phasen sind:

- **Red** — think about what you want to develop
- **Green** — think about how to make your tests pass
- **Refactor** — think about how to improve your existing implementation

![Red-Green-Refactor [codecademy - Red, Green, Refactor](https://content.codecademy.com/programs/tdd-js/articles/red-green-refactor-tdd.png)](https://content.codecademy.com/programs/tdd-js/articles/red-green-refactor-tdd.png)

\pagebreak

# FIRST-Acronym

\pagebreak

# Kent Beck (welche Rolle spielt er in Bezug auf TDD)

- erfand das Testframework SUnit
- Portierte dann SUnit mit dem Namen JUnit auf JAVA
- ist einer der Autoren des Agilen Manifests

\pagebreak

# Teststarten

## Unit-Tests (Sociable, Solitary, Mocks)

- Es werden nur einzelne Methoden getestet, nicht die ganze Klasse an sich.

## Integrationstests

-

## UI-Tests / End-To-End Tests / Systemtests

-

## Akzeptanztests

-

\pagebreak

# Testpyramide

![The Test Pyramid [The Practical Test Pyramid by martinFowler.com](https://martinfowler.com/articles/practical-test-pyramid.html)](https://martinfowler.com/articles/practical-test-pyramid/testPyramid.png)

\pagebreak

# JUNIT (Junit5)

- kennt nur zwei Ergebnisse:
  - rot: Fehlgeschlagen
  - grün: Problemlos durchgelaufen

## Codebeispiele

### Setup Methode

- läuft vor allen Tests

```java
    @BeforeAll
    public static void setUpClass() {
        // Code executed before the first test method
    }

```

### Setup Methode für jeden ausführenden Test

- läuft vor jeden einzelnen Test

```java
    @BeforeEach
    public void setUp() {
        // Code executed before each test
    }


```

### Test Methode

```java

    @Test
    public void oneThing() {

        // Code that tests one thing
    }

```

### Abschlussmethode für jeden einzelnen Test

- läuft nach jeden einzelen Test

```java
    @AfterEach
    public void tearDown() {
        // Code executed after each test
    }
```

### Abschlussmethode

- läuft nach jeden Test

```java
    @AfterAll
    public static void tearDownClass() {
        // Code executed after the last test method
    }
```

\pagebreak

# Mockito (Sinn und Funktionsweise von Mocking-Bibliotheken)

- dient zum Pseudo-Testen von Objekt-Methoden
- dient zum Erstellen von Mock-Objekten für JUnit Tests

## Beispiel

### Klasse die getestet wird

```java
   class CustomerService {
    private List<Customer> customers;

    public CustomerService(List<Customers> initialCustomers) {
        this.customers = initialCustomers;
    }

    public void addCustomer(Customer customer) {
        this.customers.add(customer);
    }

    public void deleteAllCustomers() {
        this.customers.clear();
    }
}
```

### Mockito Aufruf

```java
   // Schnittstelle zwischen Mockito-Objekt und Klassen Objekt
   List mockedCustomerList = mock(List.class);
   // Mockito-Objekt für Tests benützen
   CustomerService customerService= new CustomerService(mockedCustomerList);
    Customer customerToAdd = new Customer("Max Mustermann");
    customerService.addCustomer(customerToAdd);
    customerService.deleteAllCustomers();
    // Verifizierung der richtigen Test-Funltionsweise
    verify(mockedCustomerList).add(customerToAdd);
    verify(mockedCustomerList).clear();
```

\pagebreak

# Literatur- / Abbildungsverzeichnis

## Literaturverzeichnis

- [codecademy - Red, Green, Refactor](https://www.codecademy.com/article/tdd-red-green-refactor)
- Wikipedia
  - [EN - Kent Beck](https://en.wikipedia.org/wiki/Kent_Beck)
  - [DE - Kent Beck](https://de.wikipedia.org/wiki/Kent_Beck)
- [The Practical Test Pyramid](https://martinfowler.com/articles/practical-test-pyramid.html)
- Wikipedia
  - [EN - JUnit](https://en.wikipedia.org/wiki/JUnit)
  - [DE - JUnit](https://de.wikipedia.org/wiki/JUnit)
- [Writing Tests](https://junit.org/junit5/docs/current/user-guide/#writing-tests)
- Wikipedia
  - [EN - Mockito](https://en.wikipedia.org/wiki/Mockito)
  - [DE - Mockito](https://de.wikipedia.org/wiki/Mockito)

## Abbildungsvezeichnis

\renewcommand{\listfigurename}{}
\listoffigures
